package martinasimicic.day5;

import martinasimicic.day3.CrossedWires;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SunnyDayTest {
    @DataProvider(name = "opcodes-values")
    public Object[][] dataProviderMethod() {
        Object[][] dataInput =
                {
                        {new int[]{1002, 4, 3, 4, 33}, 0, 1, 33},
                        {new int[]{1002, 4, 3, 4, 33}, 0, 2, 3},
                        {new int[]{1101, 4, 3, 4, 33}, 0, 1, 4},
                        {new int[]{1101, 4, 3, 4, 33}, 0, 2, 3}
                };

        return dataInput;
    }

    @Test( dataProvider = "opcodes-values")
    public void getValueFor(int[] inputArray, int opcodePosition, int positionDigit, int expected) {
        int actual = SunnyDay.getValueFor(inputArray, opcodePosition, positionDigit);
        Assert.assertEquals(actual, expected);
    }
}
