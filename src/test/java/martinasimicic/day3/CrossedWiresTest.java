package martinasimicic.day3;

import martinasimicic.day2.ProgramAlarm;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CrossedWiresTest {
    @DataProvider(name = "manhattan-distances")
    public Object[][] dataProviderMethod() {
        Object[][] dataInput =
                {
                        {new String[]{"R8","U5","L5","D3"}, new String[]{"U7","R6","D4","L4"}, 6},
                        {new String[]{"R75","D30","R83","U83","L12","D49","R71","U7","L72"}, new String[]{"U62","R66","U55","R34","D71","R55","D58","R83"}, 159},
                        {new String[]{"R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"}, new String[]{"U98","R91","D20","R16","D67","R40","U7","R15","U6","R7"}, 135},
                };

        return dataInput;
    }

    @Test( dataProvider = "manhattan-distances")
    public void manhattanDistance(String[] wire1, String[] wire2, int expected) {
        int actual = CrossedWires.manhattanDistance(wire1, wire2);
        Assert.assertEquals(actual, expected);
    }


    @DataProvider(name = "steps")
    public Object[][] dataProviderStepsMethod() {
        Object[][] dataInput =
                {
                        {new String[]{"R8","U5","L5","D3"}, new String[]{"U7","R6","D4","L4"}, 30},
                        {new String[]{"R75","D30","R83","U83","L12","D49","R71","U7","L72"}, new String[]{"U62","R66","U55","R34","D71","R55","D58","R83"}, 610},
                        {new String[]{"R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"}, new String[]{"U98","R91","D20","R16","D67","R40","U7","R15","U6","R7"}, 410},
                };

        return dataInput;
    }
    @Test( dataProvider = "steps")
    public void numberOfSteps(String[] wire1, String[] wire2, int expected) {
        CrossedWires.manhattanDistance(wire1, wire2);

        String[][] wires = new String[][]{wire1, wire2};
        int actual = CrossedWires.numberOfSteps(wires);
        Assert.assertEquals(actual, expected);
    }
}
