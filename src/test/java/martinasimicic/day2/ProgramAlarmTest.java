package martinasimicic.day2;

import martinasimicic.day1.Start;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ProgramAlarmTest {
    @DataProvider(name = "computer-sequences")
    public Object[][] dataProviderMethod() {
        Object[][] dataInput =
                {
                        {new int[]{1, 0, 0, 0, 99}, new int[]{2, 0, 0, 0, 99}},
                        {new int[]{2, 3, 0, 3, 99}, new int[]{2, 3, 0, 6, 99}},
                        {new int[]{2, 4, 4, 5, 99, 0}, new int[]{2, 4, 4, 5, 99, 9801}},
                        {new int[]{1, 1, 1, 4, 99, 5, 6, 0, 99}, new int[]{30, 1, 1, 4, 2, 5, 6, 0, 99}}
                };

        return dataInput;
    }

    @Test( dataProvider = "computer-sequences")
    public void finalState(int[] value, int[] expected) {
        int[] actual = ProgramAlarm.finalState(value);
        Assert.assertEquals(actual, expected);
    }
}
