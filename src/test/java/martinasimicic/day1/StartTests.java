package martinasimicic.day1;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class StartTests {
    @DataProvider(name = "fuel-requirements-data-provider")
    public Object[][] dataProviderMethod() {
        return new Object[][] { { 12, 2 }, { 14, 2 }, { 1969, 654 }, { 100756, 33583 } };
    }

    @Test (dataProvider = "fuel-requirements-data-provider")
    public void individualFuelRequirement(int value, int expected) {
        int actual = Start.individualFuelRequirement(value);
        Assert.assertEquals(actual, expected);
    }

    @DataProvider(name = "fuel-requirements-rec-data-provider")
    public Object[][] fuelRequirementsRec() {
        return new Object[][] { { 14, 2 }, { 1969, 966 }, { 100756, 50346 } };
    }

    @Test (dataProvider = "fuel-requirements-rec-data-provider")
    public void individualRecursiveFuelRequirement(int value, int expected) {
        int actual = Start.individualRecursiveFuelRequirement(value);
        Assert.assertEquals(actual, expected);
    }
}
