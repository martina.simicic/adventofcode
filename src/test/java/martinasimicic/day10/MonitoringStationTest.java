package martinasimicic.day10;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.awt.*;

public class MonitoringStationTest {
    @DataProvider(name = "files-expectations")
    public Object[][] dataProviderMethod() {
        Object[][] dataInput =
                {
                        {"day10/test_input_1.txt", new Point(5,8)},
                        {"day10/test_input_2.txt", new Point(1,2)},
                        {"day10/test_input_3.txt", new Point(6,3)},
                        {"day10/test_input_4.txt", new Point(11,13)}
                };

        return dataInput;
    }

    @Test( dataProvider = "files-expectations")
    public void bestLocation(String file, Point expected) {
        Point actual = MonitoringStation.bestLocation(file);
        Assert.assertEquals(actual, expected);
    }
}
