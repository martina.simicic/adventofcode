package martinasimicic.day6;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class UniversalOrbitMapTest {
    @Test
    public void calculation() {
        List<String> universalOrbitData = new ArrayList<String>(List.of("COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L"));
        int actual = UniversalOrbitMap.calculation(universalOrbitData);
        Assert.assertEquals(actual, 42);
    }

    @Test
    public void calculationYOUandSAN() {
        List<String> universalOrbitData = new ArrayList<String>(List.of("COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L", "K)YOU", "I)SAN"));
        UniversalOrbitMap.calculation(universalOrbitData);
        int actual = UniversalOrbitMap.calculationYOUandSAN();
        Assert.assertEquals(actual, 4);
    }
}
