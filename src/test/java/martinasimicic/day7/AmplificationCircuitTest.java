package martinasimicic.day7;

import martinasimicic.day5.SunnyDay;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class AmplificationCircuitTest {
    @DataProvider(name = "amplifiers-data")
    public Object[][] dataProviderMethod() {
        Object[][] dataInput =
                {
                        {new ArrayList<Integer>(Arrays.asList(4,3,2,1,0)), new int[]{3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0}, 43210},
                        {new ArrayList<Integer>(Arrays.asList(0,1,2,3,4)), new int[]{3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0}, 54321},
                        {new ArrayList<Integer>(Arrays.asList(1,0,4,3,2)), new int[]{3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0}, 65210},
                };

        return dataInput;
    }

    @Test( dataProvider = "amplifiers-data")
    public void runDiagnosticsForPhaseSetting(ArrayList<Integer> phaseSetting, int[] inputArray, int expected) {
        int actual = AmplificationCircuit.runDiagnosticsForPhaseSetting(phaseSetting, inputArray);
        Assert.assertEquals(actual, expected);
    }
}
