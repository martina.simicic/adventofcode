package martinasimicic.day9;

import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import static supportTools.ScanFileToArray.importFileToIntArray;

public class SensorBoost {
    public static void boostKeycode(){
        String fileName = "day9/input.txt";
        ArrayList<Integer> inputValues = null;
        try {
            inputValues = importFileToIntArray(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        long[] inputArray = new long[inputValues.size()+4000];
        for (int i =0; i <inputValues.size(); i++){
            inputArray[i] = inputValues.get(i);
        }

        // part 1
//        int input = 1;
        // part 2
        int input = 2;
        int boost = diagnostics(input, inputArray);
        System.out.println("Relative base: " + boost);
    }

    public static int diagnostics(int input, @NotNull long[] inputArray) {
        int step = 0;
        int relativeBase = 0;

        loop:
        for (int i = 0; i < inputArray.length; i += step) {
//            System.out.println("----------");
//            System.out.println("For: " + inputArray[i] + ", " +  inputArray[i+1] + ", " +  inputArray[i+2] + ", " +  inputArray[i+3] );
            long paramOne;
            long paramTwo;
            int storeAt;

            switch ((int) inputArray[i] % 100) {
                case 99:
                    break loop;
                case 1:
                    paramOne = getValueFor(inputArray, i, 1, relativeBase);
                    paramTwo = getValueFor(inputArray, i, 2, relativeBase);
                    storeAt = getStoreAtValue(inputArray, i ,3, relativeBase);

                    inputArray[storeAt] = paramOne + paramTwo ;
                    step = 4;
                    break;
                case 2:
                    paramOne = getValueFor(inputArray, i, 1, relativeBase);
                    paramTwo = getValueFor(inputArray, i, 2, relativeBase);
                    storeAt = getStoreAtValue(inputArray, i ,3, relativeBase);

                    inputArray[storeAt] = paramOne * paramTwo ;
                    step = 4;
                    break;
                case 3:
                    storeAt = getStoreAtValue(inputArray, i ,1, relativeBase);

                    inputArray[storeAt] = input;
                    step = 2;
                    break;
                case 4:
                    System.out.println(getValueFor(inputArray, i, 1, relativeBase));
                    step = 2;
                    break;
                case 5:
                    paramOne = getValueFor(inputArray, i, 1, relativeBase);
                    paramTwo = getValueFor(inputArray, i, 2, relativeBase);

                    if (paramOne != 0){
                        i = (int) paramTwo;
                        step = 0;
                    } else {
                        step = 3;
                    }
                    break;
                case 6:
                    paramOne = getValueFor(inputArray, i, 1, relativeBase);
                    paramTwo = getValueFor(inputArray, i, 2, relativeBase);

                    if (paramOne == 0){
                        i = (int) paramTwo;
                        step = 0;
                    } else {
                        step = 3;
                    }
                    break;
                case 7:
                    paramOne = getValueFor(inputArray, i, 1, relativeBase);
                    paramTwo = getValueFor(inputArray, i, 2, relativeBase);
                    storeAt = getStoreAtValue(inputArray, i ,3, relativeBase);

                    if (paramOne < paramTwo) {
                        inputArray[storeAt] = 1;
                    } else {
                        inputArray[storeAt] = 0;
                    }
                    step = 4;
                    break;
                case 8:
                    paramOne = getValueFor(inputArray, i, 1, relativeBase);
                    paramTwo = getValueFor(inputArray, i, 2, relativeBase);
                    storeAt = getStoreAtValue(inputArray, i ,3, relativeBase);

                    if (paramOne == paramTwo) {
                        inputArray[storeAt] = 1;
                    } else {
                        inputArray[storeAt] = 0;
                    }
                    step = 4;
                    break;
                case 9:
                    relativeBase += getValueFor(inputArray, i ,1, relativeBase);
                    step = 2;
                    break;
                default:
                    System.out.println("Something is off");
                    break;
            }
        }
        return relativeBase;
    }

    public static int getStoreAtValue(long[] inputArray, int opcodePosition, int paramPosition, int relativeBase){
        long paramsCode = inputArray[opcodePosition] / 100;
        long parameterMode = ((paramsCode / (int) Math.pow(10, (paramPosition-1))) % 10);
        long storeAt = 0;

        if (parameterMode == 0) {
            storeAt = inputArray[opcodePosition + paramPosition];
        } else if (parameterMode == 2) {
            storeAt = inputArray[opcodePosition + paramPosition] + relativeBase;
        }

        return (int) storeAt;
    }

    public static long getValueFor(@NotNull long[] inputArray, int opcodePosition, int paramPosition, int relativeBase) {
        long paramsCode = inputArray[opcodePosition] / 100;
        long parameterMode = ((paramsCode / (int) Math.pow(10, (paramPosition-1))) % 10);

        // position mode
        if (parameterMode == 0){
            return inputArray[(int)inputArray[opcodePosition + paramPosition]];
        // immediate mode
        } else if (parameterMode == 1) {
            return inputArray[opcodePosition + paramPosition];
        // relative mode
        } else {
            return inputArray[(int) inputArray[opcodePosition + paramPosition] + relativeBase];
        }
    }
}
