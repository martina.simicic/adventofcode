package martinasimicic.day4;

import java.util.Arrays;
import java.util.HashMap;

public class SecurePackage {
    public static void password() {
        // 6 digits
        // within a range: 273025-767253
        // two adjecent digits are the same
        // left to right - digits never decrease - they increase or stay the same
        int currentNumber = 273025;

        int end = 767253;

        int potentialPasswordsCount = 0;

        while (currentNumber <= 767253) {
            char[] currentNumberChar = Integer.toString(currentNumber).toCharArray();

            Boolean adjacent = hasAdjacentDigits(currentNumberChar);
            Boolean increasingDigits = hasIncreasingDigits(currentNumberChar);
            Boolean followsAdjacentRules = followsAdjacentRules(adjacent, currentNumberChar);

            if (adjacent == true && increasingDigits == true && followsAdjacentRules == true) {
                potentialPasswordsCount++;
            }
            currentNumber++;
        }

        System.out.println("Potential passwords: " + potentialPasswordsCount);
    }

    public static boolean followsAdjacentRules(boolean adjacent, char[] currentNumberChar){
        if (adjacent == false) {
            return false;
        }

        HashMap<Integer, Integer> digitAndCount = new HashMap<Integer, Integer>();
        for (char digitChar : currentNumberChar) {
            int digitInt = Integer.valueOf(digitChar);
            if (digitAndCount.containsKey(digitInt)) {
                digitAndCount.put(digitInt, digitAndCount.get(digitInt).intValue() + 1);
            } else {
                digitAndCount.put(digitInt, 1);
            }
        }
        if (digitAndCount.containsValue(2)){
            return true;
        }
        return false;
    }

    public static boolean hasIncreasingDigits(char[] currentNumberChar){
        String sortedNumber = String.valueOf(sortChars(currentNumberChar));

        if (sortedNumber.equals(String.valueOf(currentNumberChar))) {
            return true;
        }
        return false;
    }

    public static boolean hasAdjacentDigits(char[] currentNumberChar){
        for (int i = 0; i < currentNumberChar.length - 1; i++) {
            if (Character.getNumericValue(currentNumberChar[i]) == Character.getNumericValue(currentNumberChar[i + 1])) {
                return true;
            }
        }
        return false;
    }

    public static char[] sortChars(char[] inputChar) {
        char[] tempCharArray = inputChar.clone();

        Arrays.sort(tempCharArray);

        return tempCharArray;
    }
}
