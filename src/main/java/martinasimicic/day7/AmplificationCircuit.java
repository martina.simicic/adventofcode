package martinasimicic.day7;

import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.ArrayList;

import static supportTools.ScanFileToArray.importFileToIntArray;

public class AmplificationCircuit {
    public static void diagnosticCode(){
        String fileName = "day7/input.txt";
        ArrayList<Integer> inputValues = null;
        try {
            inputValues = importFileToIntArray(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int[] inputArray = new int[inputValues.size()];
        for (int i =0; i <inputValues.size(); i++){
            inputArray[i] = inputValues.get(i);
        }

        System.out.println("Highest: " + highestSignalValue(inputArray));
    }

    public static int highestSignalValue(int[] inputArray) {
        int output = 0;
        int maxOutput = 0;

        ArrayList<ArrayList<Integer>> phaseSettings = permute(new int[]{0, 1, 2, 3, 4});
        for (ArrayList<Integer> phaseSetting : phaseSettings) {
            output = runDiagnosticsForPhaseSetting(phaseSetting, inputArray);

            if (output > maxOutput) {
                maxOutput = output;
            }
        }

        return maxOutput;
    }

    public static int runDiagnosticsForPhaseSetting(ArrayList<Integer> phaseSetting, int[] inputArray){
        int phaseSettingOutput = 0;

        for (Integer setting : phaseSetting) {
            int[] inputs = new int[]{setting, phaseSettingOutput};
            phaseSettingOutput = diagnostics(inputs, inputArray.clone());
        }

        return phaseSettingOutput;
    }

    public static int diagnostics(int[] inputs, @NotNull int[] inputArray) {
        int step = 0;
        int output = 0;
        int inputsIterator = 0;

        loop:
        for (int i = 0; i < inputArray.length; i += step) {
            switch (inputArray[i] % 100) {
                case 99:
                    break loop;
                case 1:
                    inputArray[inputArray[i + 3]] = getValueFor(inputArray, i, 1) + getValueFor(inputArray, i, 2) ;
                    step = 4;
                    break;
                case 2:
                    inputArray[inputArray[i + 3]] = getValueFor(inputArray, i, 1) * getValueFor(inputArray, i, 2) ;
                    step = 4;
                    break;
                case 3:
                    inputArray[inputArray[i+1]] = inputs[inputsIterator];
                    inputsIterator++;
                    step = 2;
                    break;
                case 4:
                    output = getValueFor(inputArray, i, 1);
                    System.out.println("Output: " + output);
                    step = 2;
                    break;
                case 5:
                    if (getValueFor(inputArray, i, 1) != 0){
                        i = getValueFor(inputArray, i, 2);
                        step = 0;
                    } else {
                        step = 3;
                    }
                    break;
                case 6:
                    if (getValueFor(inputArray, i, 1) == 0){
                        i = getValueFor(inputArray, i, 2);
                        step = 0;
                    } else {
                        step = 3;
                    }
                    break;
                case 7:
                    if (getValueFor(inputArray, i, 1) < getValueFor(inputArray, i, 2)) {
                        inputArray[inputArray[i+3]] = 1;
                    } else {
                        inputArray[inputArray[i+3]] = 0;
                    }
                    step = 4;
                    break;
                case 8:
                    if (getValueFor(inputArray, i, 1) == getValueFor(inputArray, i, 2)) {
                        inputArray[inputArray[i+3]] = 1;
                    } else {
                        inputArray[inputArray[i+3]] = 0;
                    }
                    step = 4;
                    break;
                default:
                    System.out.println("Something is off");
                    break;
            }
        }
        System.out.println("----------");
        System.out.println(output);

        return output;
    }

    public static int getValueFor(@NotNull int[] inputArray, int opcodePosition, int paramPosition) {
        int paramsCode = inputArray[opcodePosition] / 100;

        if (((paramsCode / (int) Math.pow(10, (paramPosition-1))) % 10) == 1){
            return inputArray[opcodePosition + paramPosition];
        } else {
            return inputArray[inputArray[opcodePosition + paramPosition]];
        }
    }

    public static ArrayList<ArrayList<Integer>> permute(int[] num) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();

        //start from an empty list
        result.add(new ArrayList<Integer>());

        for (int i = 0; i < num.length; i++) {
            //list of list in current iteration of the array num
            ArrayList<ArrayList<Integer>> current = new ArrayList<ArrayList<Integer>>();

            for (ArrayList<Integer> l : result) {
                // # of locations to insert is largest index + 1
                for (int j = 0; j < l.size()+1; j++) {
                    // + add num[i] to different locations
                    l.add(j, num[i]);

                    ArrayList<Integer> temp = new ArrayList<Integer>(l);
                    current.add(temp);

                    //System.out.println(temp);

                    // - remove num[i] add
                    l.remove(j);
                }
            }

            result = new ArrayList<ArrayList<Integer>>(current);
        }

        return result;
    }
}
