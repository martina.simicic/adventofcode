package martinasimicic.day3;

import supportTools.FileLinesToAList;

import javax.print.attribute.HashAttributeSet;
import java.awt.*;
import java.lang.reflect.Array;
import java.util.*;
import java.util.List;

public class CrossedWires {
    static CoordinatePoint[] intersectionPoints = new CoordinatePoint[50];

    public static void calculateCrossedWires(){
        String fileName = "day3/input.txt";
        List<String> inputValues = FileLinesToAList.readFileLinesToAList(fileName);

        String[] wire1Positions = inputValues.get(0).split(",");
        String[] wire2Positions = inputValues.get(1).split(",");

        System.out.println("Manhattan: " + manhattanDistance(wire1Positions, wire2Positions));
        System.out.println("Number of steps: " + numberOfSteps(new String[][]{wire1Positions, wire2Positions}));
    }

    public static int manhattanDistance(String[] wire1Positions, String[] wire2Positions) {
        Map<CoordinatePoint, String> map = new HashMap<CoordinatePoint, String>();
        Set<Map.Entry<CoordinatePoint, String>> mapSet = map.entrySet();

        CoordinatePoint startPoint = new CoordinatePoint(0, 0);
        map.put(startPoint, "S");

        plotWire(map, wire1Positions, startPoint, 1);
        int maxOccurences = plotWire(map, wire2Positions, startPoint, 2);

        int minManhattanDistance = 5_000_000;
        Set<Map.Entry<CoordinatePoint,String>> mapReader = map.entrySet();

        int i = 0;
        for (Map.Entry<CoordinatePoint, String> element: mapReader)
        {
            int ofWhichWireOne = (int) element.getValue().chars().filter(ch -> ch == '1').count();
            int ofWhichWireTwo = (int) element.getValue().chars().filter(ch -> ch == '2').count();

            if (element.getValue().length() == maxOccurences && ofWhichWireOne == ofWhichWireTwo) {
                CrossedWires.intersectionPoints[i] = element.getKey();
                i++;
                int currentManhattanDistance = Math.abs(element.getKey().x) + Math.abs(element.getKey().y);
                if (currentManhattanDistance < minManhattanDistance){
                    minManhattanDistance = currentManhattanDistance;
                }
            }
        }
        return minManhattanDistance;
    }

    public static int numberOfSteps(String[][] wirePositions){
        HashMap<CoordinatePoint, Integer> wireSteps = new HashMap<CoordinatePoint, Integer>();
        intersectionLoop:
        for (int intersectionCounter = 1; intersectionCounter < intersectionPoints.length; intersectionCounter++) {
            if (intersectionPoints[intersectionCounter] == null) {
                break intersectionLoop;
            }
            int steps = 0;
            for (int i = 0; i < wirePositions.length; i++) {
                CoordinatePoint localStartPoint = new CoordinatePoint(0, 0);
                wireLoop:
                for (int j = 0; j < wirePositions[i].length; j++) {
                    String direction = wirePositions[i][j].substring(0, 1);
                    int incrementFor = Integer.parseInt(wirePositions[i][j].substring(1, wirePositions[i][j].length()));
                    int incrementStepX = 0;
                    int incrementStepY = 0;
                    switch (direction) {
                        case "R":
                            incrementStepX = 1;
                            break;
                        case "L":
                            incrementStepX = -1;
                            break;
                        case "U":
                            incrementStepY = 1;
                            break;
                        case "D":
                            incrementStepY = -1;
                            break;
                    }

                    if (incrementStepX == -1 || incrementStepX == 1) {
                        int startX = localStartPoint.x + incrementStepX * 1;
                        for (int x = 1; x <= incrementFor; x++) {
                            steps++;
                            CoordinatePoint coordinate = new CoordinatePoint(startX + incrementStepX * x, localStartPoint.y);

                            if (coordinate.x == intersectionPoints[intersectionCounter].x * (-1) &&
                                    coordinate.y == intersectionPoints[intersectionCounter].y * (-1)) {
                                steps++;
                                break wireLoop;
                            }
                        }
                        localStartPoint = new CoordinatePoint(localStartPoint.x + incrementStepX * incrementFor, localStartPoint.y);
                    } else if (incrementStepY == 1 || incrementStepY == -1) {
                        int startY = localStartPoint.y + incrementStepY * 1;
                        for (int y = 1; y <= incrementFor; y++) {
                            steps++;
                            CoordinatePoint coordinate = new CoordinatePoint(localStartPoint.x, startY + incrementStepY * y);

                            if (coordinate.x == intersectionPoints[intersectionCounter].x * (-1) &&
                                    coordinate.y == intersectionPoints[intersectionCounter].y * (-1)){
                                steps++;
                                break wireLoop;
                            }
                        }
                        localStartPoint = new CoordinatePoint(localStartPoint.x, localStartPoint.y + incrementStepY * incrementFor);
                    }

                }
                System.out.println(steps);
            }
            int value = steps;
            if (wireSteps.containsKey(intersectionPoints[intersectionCounter])) {
                value = steps + wireSteps.get(intersectionPoints[intersectionCounter]);
            }
            wireSteps.put(intersectionPoints[intersectionCounter], value);
            steps = 0;
        }

        int minNumberOfSteps = 5_000_000;
        Set<Map.Entry<CoordinatePoint,Integer>> mapReader = wireSteps.entrySet();
        for (Map.Entry<CoordinatePoint, Integer> element: mapReader)
        {
            if (element.getValue() < minNumberOfSteps) {
                minNumberOfSteps = element.getValue();
            }
        }
        return minNumberOfSteps;
    }

    private static int plotWire(Map map, String[] wirePositions, CoordinatePoint startPoint, int wireNumber){
        CoordinatePoint localStartPoint = new CoordinatePoint(startPoint.x, startPoint.y);
        int maxValue = 0;
        for (int i = 0; i < wirePositions.length; i++) {
            String direction = wirePositions[i].substring(0,1);
            int incrementFor = Integer.parseInt(wirePositions[i].substring(1, wirePositions[i].length()));
            int incrementStepX = 0;
            int incrementStepY = 0;
            switch (direction) {
                case "R":
                    incrementStepX = 1;
                    break;
                case "L":
                    incrementStepX = -1;
                    break;
                case "U":
                    incrementStepY = 1;
                    break;
                case "D":
                    incrementStepY = -1;
                    break;
            }


            if (incrementStepX == -1 || incrementStepX == 1) {
                int startX = localStartPoint.x - incrementStepX * 1;
                for ( int x = 1; x <= incrementFor; x++) {
                    int value = incrementPosition(map,startX- incrementStepX * x , localStartPoint.y, wireNumber);
                    if (value > maxValue) {
                        maxValue = value;
                    }
                }
                localStartPoint = new CoordinatePoint(localStartPoint.x -incrementStepX * incrementFor, localStartPoint.y);
            } else if (incrementStepY == 1 || incrementStepY == -1) {
                int startY = localStartPoint.y - incrementStepY * 1;
                for (int y = 1; y <= incrementFor; y++) {
                    int value = incrementPosition(map, localStartPoint.x, startY- incrementStepY * y, wireNumber);
                    if (value > maxValue) {
                        maxValue = value;
                    }
                }
                localStartPoint = new CoordinatePoint(localStartPoint.x, localStartPoint.y - incrementStepY * incrementFor);
            }
        }
        return maxValue;
    }

    private static int incrementPosition(Map<CoordinatePoint, String> map, int x, int y, int wireNumber) {
        CoordinatePoint coordinate = new CoordinatePoint(x,y);
        String value = "";
        if (map.containsKey(coordinate)) {
            value = map.get(coordinate).concat(String.valueOf(wireNumber));
        } else {
            value = "" + wireNumber;
        }
        map.put(coordinate, ""+value);
        return value.length();
    }
}
