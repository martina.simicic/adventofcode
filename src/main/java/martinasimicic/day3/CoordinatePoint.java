package martinasimicic.day3;

import java.awt.*;

class CoordinatePoint extends Point {
    public CoordinatePoint() {
        this(0, 0);
    }

    public CoordinatePoint(Point p) {
        this(p.x, p.y);
    }

    public CoordinatePoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int hashCode()
    {
        return 31 * x + y;
    }

    @Override
    public boolean equals(final Object o)
    {
        // No object instance is equal to null
        if (o == null)
            return false;
        // If the object is the same, this is true
        if (this == o)
            return true;
        // If not the same object class, false
        if (getClass() != o.getClass())
            return false;

        final Point other = (Point) o; // safe to cast since getClass() == o.getClass()
        return x == other.x && y == other.y; // test instance member equality
    }
}
