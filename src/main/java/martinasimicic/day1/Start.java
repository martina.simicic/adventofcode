package martinasimicic.day1;

import java.util.Iterator;
import java.util.List;

import static supportTools.FileLinesToAList.readFileLinesToAList;

public class Start {
    public static void calculateFuelRequirement() {
        String fileName = "day1/input.txt";
        List<String> inputValues = readFileLinesToAList(fileName);

        int result1 = totalFuelRequirement(inputValues);
        int result2 = totalRecursiveFuelRequirement(inputValues);

        System.out.println("Full: " + result1);
        System.out.println("Recursive: " + result2);
    }


    public static int totalFuelRequirement(List<String> inputValues) {
        int total = 0;

        Iterator<String> iterator = inputValues.iterator();
        while (iterator.hasNext()) {
            int inputInt = Integer.parseInt(iterator.next());
            total += individualFuelRequirement(inputInt);
        }

        return total;
    }

    public static int totalRecursiveFuelRequirement(List<String> inputValues) {
        int total = 0;

        Iterator<String> iterator = inputValues.iterator();
        while (iterator.hasNext()) {
            int inputInt = Integer.parseInt(iterator.next());
            total += individualRecursiveFuelRequirement(inputInt);
        }

        return total;
    }

    public static int individualFuelRequirement(int mass) {
        return mass / 3 - 2;
    }

    public static int individualRecursiveFuelRequirement(int mass) {
        int requirement = mass / 3 - 2;

        if (requirement < 0) {
            return 0;
        }
        return requirement + individualRecursiveFuelRequirement(requirement);
    }
}