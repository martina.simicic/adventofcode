package martinasimicic.day8;

import martinasimicic.day1.Start;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;

public class SpaceImageProgram {
    public static HashMap<Integer, int[][]> layers = new HashMap<Integer, int[][]>();

    public static void processImage() throws IOException, URISyntaxException {
        URL res = Start.class.getClassLoader().getResource("day8/input.txt");
        File file = null;
        file = Paths.get(res.toURI()).toFile();

        loadImageLayers(file);

        //        0 - black
        //        1 - white
        //        2 - transparent
        int[][] drawing = new int[6][25];
        // first layer in front and last in back
        // if back (last el) dark - it will go through

        for (int row = 0; row < 6; row++) {
            for (int col = 0; col < 25; col++) {
                int color = 4;
                drawing[row][col] = layers.get(0)[row][col];

//                System.out.println("For position: " + row + "," + col);
                for (int layer = 1; layer < layers.size(); layer++){
                    if (drawing[row][col] == 0){
                        color = 0;
                    } else if (drawing[row][col] == 2){
                        color = layers.get(layer)[row][col];
                    } else {
                        color = 1;
                    }
//                    System.out.println("Default value: " + drawing[row][col] + " to be: " + layers.get(layer)[row][col] + " decided on: " + color);
                    drawing[row][col] = color;
                }
            }
        }

//        for (int row = 0; row < 6; row++) {
//            for (int col = 0; col < 25; col++) {
//                for (int layer = layers.size()-1; layer >= 0; layer--){
//                    if (layers.get(layer)[row][col] == 0){
//                        drawing[row][col] = 0;
//                    } else if (layers.get(layer)[row][col] == 1){
//                        drawing[row][col] = 1;
//                    }
//                }
//            }
//        }

        for (int row = 0; row < 6; row++) {
            for (int col = 0; col < 25; col++) {
                switch (drawing[row][col]){
                    case 0:
                        System.out.print(" ");
                        break;
                    case 1:
                        System.out.print("#");
                        break;
                    case 2:
                        System.out.print(".");
                        break;
                }
            }
            System.out.print("\n");
        }
    }

    public static void loadImageLayers(File file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file.getPath()));
        int pixel;
        int layerCounter = 0;
        int rowCounter = 0;
        int columnCounter = 0;

        HashMap<Integer, HashMap> layersDigits = new HashMap<Integer, HashMap>();

        int[][] image = new int[6][25];
        int zeros = 0;
        int ones = 0;
        int twos = 0;

        int pixelCounter = 0;
        while ((pixel = reader.read()) != -1) {
            int pixelInt = Character.getNumericValue(pixel);

            switch (pixelInt){
                case 0:
                    zeros++;
                    break;
                case 1:
                    ones++;
                    break;
                case 2:
                    twos++;
                    break;
            }

            if (columnCounter >= 25) {
                columnCounter = 0;
                rowCounter++;
            }

            if (rowCounter < 6) {
                image[rowCounter][columnCounter] = pixelInt;
                columnCounter++;
            } else {
                layers.put(layerCounter, image.clone());

                int finalZeros = zeros;
                int finalOnes = ones;
                int finalTwos = twos;
                HashMap<String,Integer> digitCountersTotal = new HashMap<String, Integer>()
                {
                    {
                        put("zeros", finalZeros);
                        put("ones", finalOnes);
                        put("twos", finalTwos);
                    }
                };

                layersDigits.put(layerCounter, digitCountersTotal);
                zeros = 0;
                ones = 0;
                twos = 0;
                layerCounter++;
                image = new int[6][25];
                rowCounter = 0;
                columnCounter = 0;

                image[rowCounter][columnCounter] = pixelInt;
                columnCounter++;
            }
            pixelCounter++;
        }

        int layerWithMinZeros = -1;
        int forThatCountOnes = -1;
        int forThatCountTwos = -1;
        int minZeros = 5_000;
        Iterator iterator = layersDigits.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry pair = (Map.Entry)iterator.next();
            if ((int)((HashMap) pair.getValue()).get("zeros") < minZeros) {
                minZeros = (int)((HashMap) pair.getValue()).get("zeros");
                layerWithMinZeros = (int) pair.getKey();
                forThatCountOnes = (int)((HashMap) pair.getValue()).get("ones");
                forThatCountTwos = (int)((HashMap) pair.getValue()).get("twos");
            }
        }

        System.out.println("Layer with least zeros is: " + layerWithMinZeros + " and ones * twos = " + forThatCountOnes*forThatCountTwos);
    }
}
