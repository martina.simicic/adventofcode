package martinasimicic.day6;

import java.util.ArrayList;
import java.util.List;

public class Tree {
        public Node root;

        public Tree(Node root) {
           this.root = root;
        }

        public String rootLabel(){
            return root.label();
        }
}

