package martinasimicic.day6;

import supportTools.FileLinesToAList;

import java.util.*;

// TODO: Refactor all the loops and do it smarter!
public class UniversalOrbitMap {
    public static ArrayList<Node> allNodes = new ArrayList<>();

    public static void numberOfOrbits(){
        String fileName = "day6/input.txt";
        List<String> inputValues = FileLinesToAList.readFileLinesToAList(fileName);

        System.out.println("Paths number: " + calculation(inputValues));

        System.out.println("Shortest: " + calculationYOUandSAN());
    }

    public static int calculationYOUandSAN(){
        ArrayList<String> sanParents = getParentsForSan();

        Node currentMe = null;
        for (Node node : allNodes){
            if (node.label.equals("YOU")){
                currentMe = node;
            }
        }
        int sum = 0;
        String ancestor = "";
        while (currentMe.parent != null) {
            if (sanParents.contains(currentMe.label)) {
                ancestor = currentMe.label;
                break;
            }
            sum++;
            currentMe = currentMe.parent;
        }

        Node santa = null;
        for (Node node : allNodes) {
            if (node.label.equals("SAN")) {
                santa = node;
            }
        }

        int santa_sum = 0;
        while (!santa.label.equals(ancestor)) {
            santa_sum++;
            santa = santa.parent;
        }


        return sum + santa_sum - 2;
    }

    public static ArrayList<String> getParentsForSan() {
        Node santa = null;
        for (Node node : allNodes) {
            if (node.label.equals("SAN")) {
                santa = node;
            }
        }

        ArrayList<String> sanParents = new ArrayList<>();
        while (santa != null && santa.parent != null) {
            sanParents.add(santa.parent.label);
            santa = santa.parent;
        }

        return sanParents;
    }

    public static int calculation(List<String> inputValues){
        String[] firstElement = inputValues.get(0).split("\\)");

        Node rootNode = new Node(firstElement[0]);
        allNodes.add(rootNode);
        Tree tree = new Tree(rootNode);

        Node node = new Node(firstElement[1]);
        allNodes.add(node);
        node.parent = rootNode;
        rootNode.addChild(node);

        for (int i=1; i < inputValues.size(); i++){
            String[] inputs = inputValues.get(i).split("\\)");

            Node parent = null;
            Node child = null;

            Boolean childToAdd = false;
            Boolean parentToAdd = false;

            for(int j = 0; j < allNodes.size(); j++) {
                if (allNodes.get(j).label.equals(inputs[0])) {
                    parent = allNodes.get(j);
                    break;
                }
            }
            if (parent == null){
                parent = new Node(inputs[0]);
                parentToAdd = true;
            }

            for(int j = 0; j < allNodes.size(); j++) {
                if (allNodes.get(j).label.equals(inputs[1])) {
                    child = allNodes.get(j);
                    break;
                }
            }

            if (child == null){
                child = new Node(inputs[1]);
                childToAdd = true;
            }

            child.parent = parent;
            parent.addChild(child);

            if (parentToAdd.equals(true)) {
                allNodes.add(parent);
            }
            if (childToAdd.equals(true)) {
                allNodes.add(child);
            }
        }

        int sum = 0;
        for (Node nodeElement : allNodes) {
            sum += countParentPaths(nodeElement);
        }

        return sum;
    }

    private static int countParentPaths(Node nodeElement) {
        if (nodeElement.parent == null) {
            return 0;
        }

        return 1 + countParentPaths(nodeElement.parent);
    }
}

