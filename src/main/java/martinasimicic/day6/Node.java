package martinasimicic.day6;

import java.util.ArrayList;

public class Node {
    public String label;
    public Node parent;
    public ArrayList<Node> children = new ArrayList<>();

    public Node(String label){
        this.label = label;
    }

    public String label() {
        return label;
    }

    public void addChild(Node child) {
        this.children.add(child);
    }
}
