package martinasimicic.day2;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;

import static supportTools.ScanFileToArray.importFileToIntArray;

public class ProgramAlarm {
    public static void calculateProgramAlarm() {
        String fileName = "day2/input.txt";
        ArrayList<Integer> inputValues = null;
        try {
            inputValues = importFileToIntArray(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

//        System.out.println(inputValues);
        int[] inputArray = new int[inputValues.size()];
        for (int i =0; i <inputValues.size(); i++){
            inputArray[i] = inputValues.get(i);
        }

        int[] restoredComputer = finalState(inputArray.clone(), 12, 2);

        System.out.println("Value on position 0: " + restoredComputer[0]);
        System.out.println("Value of noun/verb combination: " + finalStateWithMoreParameters(inputArray.clone()));
    }

    public static int[] finalState(int[] inputArray) {
        return finalState(inputArray, inputArray[1], inputArray[2]);
    }

    public static int[] finalState(int[] inputArray, int valueAddressOne, int valueAddressTwo) {
        inputArray[1] = valueAddressOne;
        inputArray[2] = valueAddressTwo;

        for (int i = 0; i < inputArray.length; i += 4) {
            int[] subset = Arrays.copyOfRange(inputArray, i, i + 4);
            switch (subset[0]) {
                case 99:
                    return inputArray;
                case 1:
                    inputArray[subset[3]] = inputArray[subset[1]] + inputArray[subset[2]];
                    break;
                case 2:
                    inputArray[subset[3]] = inputArray[subset[1]] * inputArray[subset[2]];
                    break;
                default:
                    System.out.println("Something is off");
                    break;
            }
        }
        return inputArray;
    }

    public static int finalStateWithMoreParameters(int[] inputArray) {
        for (int i= 0; i <= 99; i++){
            for (int j = 0; j <= 99; j++ ) {
                if (finalState(inputArray.clone(), i, j)[0] == 19690720) {
                    return 100 * i + j;
                }
            }
        }
        return 0;
    }
}
