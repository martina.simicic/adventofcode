package martinasimicic.day5;

import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import static supportTools.ScanFileToArray.importFileToIntArray;

public class SunnyDay {
    public static void diagnosticCode(){
        String fileName = "day5/input.txt";
//        String fileName = "day5/test_input.txt";
        ArrayList<Integer> inputValues = null;
        try {
            inputValues = importFileToIntArray(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int[] inputArray = new int[inputValues.size()];
        for (int i =0; i <inputValues.size(); i++){
            inputArray[i] = inputValues.get(i);
        }

//        int input = 1;
//        diagnostics(input, inputArray);
        int input = 5;
        diagnostics(input, inputArray);
    }

    public static int[] diagnostics(int input, @NotNull int[] inputArray) {
        int step = 0;

        for (int i = 0; i < inputArray.length; i += step) {
            System.out.println("----------");
            System.out.println("For: " + inputArray[i] + ", " +  inputArray[i+1] + ", " +  inputArray[i+2] + ", " +  inputArray[i+3] );
            switch (inputArray[i] % 100) {
                case 99:
                    return inputArray;
                case 1:
                    inputArray[inputArray[i + 3]] = getValueFor(inputArray, i, 1) + getValueFor(inputArray, i, 2) ;
                    step = 4;
                    break;
                case 2:
                    inputArray[inputArray[i + 3]] = getValueFor(inputArray, i, 1) * getValueFor(inputArray, i, 2) ;
                    step = 4;
                    break;
                case 3:
                    inputArray[inputArray[i+1]] = input;
                    step = 2;
                    break;
                case 4:
                    System.out.println(getValueFor(inputArray, i, 1));
                    step = 2;
                    break;
                case 5:
                    if (getValueFor(inputArray, i, 1) != 0){
                        i = getValueFor(inputArray, i, 2);
                        step = 0;
                    } else {
                        step = 3;
                    }
                    break;
                case 6:
                    if (getValueFor(inputArray, i, 1) == 0){
                        i = getValueFor(inputArray, i, 2);
                        step = 0;
                    } else {
                        step = 3;
                    }
                    break;
                case 7:
                    if (getValueFor(inputArray, i, 1) < getValueFor(inputArray, i, 2)) {
                        inputArray[inputArray[i+3]] = 1;
                    } else {
                        inputArray[inputArray[i+3]] = 0;
                    }
                    step = 4;
                    break;
                case 8:
                    if (getValueFor(inputArray, i, 1) == getValueFor(inputArray, i, 2)) {
                        inputArray[inputArray[i+3]] = 1;
                    } else {
                        inputArray[inputArray[i+3]] = 0;
                    }
                    step = 4;
                    break;
                default:
                    System.out.println("Something is off");
                    break;
            }
        }
        return inputArray;
    }

    public static int getValueFor(@NotNull int[] inputArray, int opcodePosition, int paramPosition) {
        int paramsCode = inputArray[opcodePosition] / 100;

        if (((paramsCode / (int) Math.pow(10, (paramPosition-1))) % 10) == 1){
            return inputArray[opcodePosition + paramPosition];
        } else {
            return inputArray[inputArray[opcodePosition + paramPosition]];
        }
    }
}
