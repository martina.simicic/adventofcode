package martinasimicic.day10;

import java.awt.*;
import java.util.*;
import java.util.List;

import static supportTools.FileLinesToAList.readFileLinesToAList;

public class MonitoringStation {
    public static HashMap<Point, ArrayList<Double>> coordinateAtan2Values = new HashMap<Point, ArrayList<Double>>();
    public static ArrayList<Angle> angles = new ArrayList<Angle>();
    public static HashMap<Double, Map<Point, Point>> atanCoordinateCollection = new HashMap<Double, Map<Point, Point>>();

    public static void findBest(){
        String fileName = "day10/input.txt";
//        String fileName = "day10/test_input_4.txt";
        Point monitoringStationLocation = bestLocation(fileName);
        System.out.println("Best location: " + monitoringStationLocation.x + "," + monitoringStationLocation.y);

        Point unluckyAsteroid = vaporizationOnCount200(monitoringStationLocation);
        System.out.println("Asteroid vaporized, iteration 200: " + unluckyAsteroid.x + "," + unluckyAsteroid.y);

        // 1907 too high
        // 911, 920, 515, 215
        double response = unluckyAsteroid.x*100 + unluckyAsteroid.y;
        System.out.println("Response for part 2 is: " + response);
    }

    public static Point vaporizationOnCount200(Point monitoringLocation) {
        // get only distances for monitoringLocation
        ArrayList<Angle> anglesForMonitoringLocation = new ArrayList<Angle>();
        for (Angle angle : angles) {
            if (angle.source.equals(monitoringLocation)){
                anglesForMonitoringLocation.add(angle);
            }
        }
        Collections.sort(anglesForMonitoringLocation);
//
//        for (Angle angle : anglesForMonitoringLocation) {
//            System.out.println(("Destination " + angle.destination.x + "," + angle.destination.y + " with value: " + angle.atan2));
//        }

        int nth = 1;
        int i = 1;
        double pastValue = anglesForMonitoringLocation.get(i).atan2;
        anglesForMonitoringLocation.get(i).shotDown = true;

        while (nth < 200) {
            int position = (i + anglesForMonitoringLocation.size()) % anglesForMonitoringLocation.size();
            Angle currentAngle = anglesForMonitoringLocation.get(position);

            if (currentAngle.shotDown == true || Math.abs(pastValue - anglesForMonitoringLocation.get(position).atan2) <= 0.000001){
//                System.out.println("The same, skipping " + anglesForMonitoringLocation.get(position).destination + " value: " + anglesForMonitoringLocation.get(position).atan2);
                i++;
            } else {
//                System.out.println("Shooting down " + anglesForMonitoringLocation.get(position).destination);
                pastValue = currentAngle.atan2;
                currentAngle.shotDown = true;
                nth++;
                i++;
            }
        }

        Point point = anglesForMonitoringLocation.get((i - 1 + anglesForMonitoringLocation.size()) % anglesForMonitoringLocation.size()).destination;

        return point;
    }

    public static Point bestLocation(String fileName){
        HashMap<Point, Boolean> spaceCoordinates = loadSpaceCoordinates(fileName);
        // for each asteroid find atan2 value
        // find unique values
        // coordinate with max unique values is the one
        for (Map.Entry<Point, Boolean> pointEntry : spaceCoordinates.entrySet()) {
            if (pointEntry.getValue().equals(true)) {
                coordinateAtan2Values.put(pointEntry.getKey(), new ArrayList<Double>());

                // atan2 for other distances
                for (Map.Entry<Point, Boolean> comparablePointEntry : spaceCoordinates.entrySet()) {
                    // skip self
                    if (pointEntry.equals(comparablePointEntry) || comparablePointEntry.getValue().equals(false)) {
                        continue;
                    }

                    // calculate atan2 and store for coordinate
                    int precision = 1_000_000_000;
                    double baseAtanValue = Math.atan2(comparablePointEntry.getKey().y-pointEntry.getKey().y, comparablePointEntry.getKey().x-pointEntry.getKey().x);
                    double adjustedAtanValue = ((baseAtanValue + Math.PI / 2) + 2 * Math.PI) % (2 * Math.PI);
                    double atanValueRounded = Math.floor(adjustedAtanValue * precision +.5)/precision;
                    Angle angle = new Angle(pointEntry.getKey(), comparablePointEntry.getKey(), atanValueRounded);
                    angles.add(angle);

                    if (coordinateAtan2Values.get(pointEntry.getKey()).contains(atanValueRounded)) {
                        continue;
                    } else {
                        atanCoordinateCollection.put(atanValueRounded, new HashMap<Point, Point>(){{ put(pointEntry.getKey(), comparablePointEntry.getKey()); }});
                        coordinateAtan2Values.get(pointEntry.getKey()).add(atanValueRounded);
                    }
                }
            }
        }

        // find the one with max
        Point maxPoint = new Point(-1, -1);
        int maxSize = 0;
        for (Map.Entry<Point, ArrayList<Double>> pointArrayAtans : coordinateAtan2Values.entrySet()) {
            if (pointArrayAtans.getValue().size() > maxSize) {
                maxSize = pointArrayAtans.getValue().size();
                maxPoint = pointArrayAtans.getKey();
            }
        }

        System.out.println("Max point: " + maxPoint.x + "," + maxPoint.y + " and from there visible: " + maxSize);
        return maxPoint;
    }

    public static HashMap<Point, Boolean> loadSpaceCoordinates(String fileName) {
        HashMap<Point, Boolean> spaceCoordinates = new HashMap<Point, Boolean>();

        List<String> inputValues = readFileLinesToAList(fileName);

        int inputValuesCounter = 0;
        for (String line : inputValues){
            for(int x = 0; x < line.length(); x++){
                boolean hasAsteroid = false;

                if (String.valueOf(line.charAt(x)).equals("#")){
                    hasAsteroid = true;
                }

                spaceCoordinates.put(new Point(x, inputValuesCounter), hasAsteroid);
            }

            inputValuesCounter++;
        }

        return spaceCoordinates;
    }
}
