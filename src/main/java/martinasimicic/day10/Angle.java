package martinasimicic.day10;

import java.awt.*;

public class Angle implements Comparable<Angle> {
    public double atan2;
    public Point source;
    public Point destination;
    public boolean shotDown = false;

    public Angle(Point source, Point destination, double atan2){
        this.source = source;
        this.destination = destination;
        this.atan2 = atan2;
    }

    @Override
    public int compareTo(Angle a) {
        return new Double(atan2).compareTo( a.atan2);
    }
    @Override
    public String toString() {
        return String.valueOf(atan2);
    }
}
