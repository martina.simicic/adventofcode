package supportTools;

import martinasimicic.day1.Start;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class ScanFileToArray {
    public static ArrayList<Integer> importFileToIntArray(String fileName) throws FileNotFoundException {
        ArrayList<Integer> content = new ArrayList<Integer>();
        String[] lines = importFileToArray(fileName).split(",");

        for (int i=0; i<lines.length; i++){
            content.add(Integer.parseInt(lines[i]));
        }

        return content;
    }
    
    public static String[] importFileToStringArray(String fileName) throws FileNotFoundException {
        return importFileToArray(fileName).split(",");
    }

    private static String importFileToArray(String fileName) throws FileNotFoundException {
        URL res = Start.class.getClassLoader().getResource(fileName);
        File file = null;
        try {
            file = Paths.get(res.toURI()).toFile();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        Scanner sc = new Scanner(file);

        return sc.next();
    }
}
